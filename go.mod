module gitlab.com/wiky.lyu/echo-starter

go 1.14

require (
	github.com/aws/aws-sdk-go v1.42.33 // indirect
	github.com/bketelsen/crypt v0.0.3-0.20200106085610-5cbc8cc4026c // indirect
	github.com/coreos/bbolt v1.3.2 // indirect
	github.com/coreos/go-systemd v0.0.0-20190321100706-95778dfbb74e // indirect
	github.com/coreos/pkg v0.0.0-20180928190104-399ea9e2e55f // indirect
	github.com/frankban/quicktest v1.14.0 // indirect
	github.com/go-pg/pg/v10 v10.10.6
	github.com/go-playground/validator/v10 v10.10.0
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/gobuffalo/genny v0.1.1 // indirect
	github.com/gobuffalo/gogen v0.1.1 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/btree v1.0.1 // indirect
	github.com/gorilla/sessions v1.2.1
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hashicorp/consul/api v1.12.0
	github.com/hashicorp/go-hclog v1.1.0 // indirect
	github.com/hashicorp/serf v0.9.7 // indirect
	github.com/karrick/godirwalk v1.10.3 // indirect
	github.com/klauspost/compress v1.14.1 // indirect
	github.com/labstack/echo-contrib v0.11.0
	github.com/labstack/echo/v4 v4.6.3
	github.com/prometheus/tsdb v0.7.1 // indirect
	github.com/rogpeppe/go-internal v1.8.1 // indirect
	github.com/shabbyrobe/xmlwriter v0.0.0-20210324110748-440e98cf0c87 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/afero v1.8.0 // indirect
	github.com/spf13/viper v1.10.1
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/tealeg/xlsx/v3 v3.2.4
	github.com/tmc/grpc-websocket-proxy v0.0.0-20190109142713-0ad062ec5ee5 // indirect
	github.com/uber-go/atomic v1.4.0 // indirect
	github.com/vmihailenco/msgpack/v4 v4.3.11 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	github.com/xdg/scram v1.0.3 // indirect
	github.com/xdg/stringprep v1.0.3 // indirect
	github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a // indirect
	go.mongodb.org/mongo-driver v1.8.2
	go.opentelemetry.io/otel/internal/metric v0.26.0 // indirect
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce // indirect
	golang.org/x/net v0.0.0-20220111093109-d55c255bac03 // indirect
	golang.org/x/sys v0.0.0-20220111092808-5a964db01320 // indirect
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11 // indirect
)
