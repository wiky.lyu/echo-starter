package rbac

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/wiky.lyu/echo-starter/db"
)

func GetAPIByMethodAndPath(method, path string) (*API, error) {
	pg := db.PG()

	api := API{}
	if err := pg.Model(&api).Where(`"api"."method"=?`, method).Where(`"api"."path"=?`, path).Select(); err != nil {
		if err != db.ErrNoRows {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &api, nil
}

func FindAPIs(query, method string, page, pageSize int) ([]*API, int, error) {
	pg := db.PG()

	apis := make([]*API, 0)
	q := pg.Model(&apis).Relation(`Perm`)
	if query != "" {
		q = q.Where(`"api"."path" LIKE ?`, "%"+query+"%")
	}
	if method != "" {
		q = q.Where(`"api"."method"=?`, method)
	}

	total, err := q.Limit(pageSize).Offset((page - 1) * pageSize).Order(`id DESC`).SelectAndCount()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, 0, err
	}
	return apis, total, nil
}

func CreateAPI(method, path string, permID int64, createdBy int64) (*API, error) {
	pg := db.PG()
	tx, err := pg.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	perm := Permission{ID: permID}
	if err := tx.Model(&perm).WherePK().Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	api := API{
		Method:    method,
		Path:      path,
		PermID:    perm.ID,
		CreatedBy: createdBy,
	}

	if _, err := tx.Model(&api).Insert(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return &api, nil
}

func UpdateAPI(id int64, method, path string, permID int64) (*API, error) {
	pg := db.PG()
	tx, err := pg.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	perm := Permission{ID: permID}
	if err := tx.Model(&perm).WherePK().Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	api := API{ID: id}
	if err := tx.Model(&api).WherePK().Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if _, err := tx.Model(&api).WherePK().Set(`"updated_at"=CURRENT_TIMESTAMP`).
		Set(`"method"=?`, method).Set(`"path"=?`, path).
		Set(`"perm_id"=?`, perm.ID).Returning(`*`).Update(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return &api, nil
}
