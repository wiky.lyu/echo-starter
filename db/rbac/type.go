package rbac

import (
	"time"
)

type Permission struct {
	tableName struct{} `pg:"permission" json:"-"`
	ID        int64    `pg:"id,pk" json:"id"`

	Name     string `pg:"name,notnull" json:"name"`
	Code     string `pg:"code,notnull" json:"code"`
	FullCode string `pg:"fullcode,notnull" json:"fullcode"`
	ParentID int64  `pg:"parent_id,notnull,use_zero" json:"parent_id"`
	IsMenu   bool   `pg:"is_menu,notnull,use_zero" json:"is_menu"`
	Sort     int    `pg:"sort,notnull,use_zero" json:"sort"`

	CreatedBy int64     `pg:"created_by" json:"created_by"`
	CreatedAt time.Time `pg:"created_at" json:"created_at"`
	UpdatedAt time.Time `pg:"updated_at" json:"updated_at"`

	Children []*Permission `pg:"-" json:"children"`
}

type API struct {
	tableName struct{} `pg:"api" json:"-"`

	ID     int64  `pg:"id,pk" json:"id"`
	Method string `pg:"method,notnull" json:"method"`
	Path   string `pg:"path,notnull" json:"path"`
	PermID int64  `pg:"perm_id,notnull" json:"perm_id"`

	CreatedBy int64     `pg:"created_by" json:"created_by"`
	CreatedAt time.Time `pg:"created_at" json:"created_at"`
	UpdatedAt time.Time `pg:"updated_at" json:"updated_at"`

	Perm *Permission `pg:"rel:has-one" json:"perm,omitempty"`
}
