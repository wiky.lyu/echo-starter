package rbac

import (
	"fmt"
	"strings"

	"github.com/go-pg/pg/v10"
	log "github.com/sirupsen/logrus"
	"gitlab.com/wiky.lyu/echo-starter/db"
	"gitlab.com/wiky.lyu/echo-starter/x"
)

func findPermissionChildren(perm *Permission) error {
	children, err := FindPermissionsByParent(perm.ID)
	if err != nil {
		return err
	}
	if len(children) > 0 {
		for _, child := range children {
			if err := findPermissionChildren(child); err != nil {
				return err
			}
		}
	}
	perm.Children = children
	return nil
}

func FindPermissionTree() ([]*Permission, error) {
	perms, err := FindPermissionsByParent(0)
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	for _, perm := range perms {
		if err := findPermissionChildren(perm); err != nil {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
	}
	return perms, nil
}

func FindPermissionsByParent(parentID int64) ([]*Permission, error) {
	pg := db.PG()

	perms := make([]*Permission, 0)
	if err := pg.Model(&perms).Where(`"parent_id"=?`, parentID).Order(`sort`).Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return perms, nil
}

func FindPermissions(query string, parentID int64, page, pageSize int) ([]*Permission, int, error) {
	pg := db.PG()

	perms := make([]*Permission, 0)

	q := pg.Model(&perms).Order(`id`)
	if query != "" {
		if x.IsChinese(query) {
			q = q.Where(`"name" LIKE ?`, "%"+query+"%")
		} else if strings.Contains(query, ".") {
			q = q.Where(`"fullcode"=?`, query)
		} else {
			q = q.Where(`"code"=?`, query)
		}
	}
	if parentID >= 0 {
		q = q.Where(`"parent_id"=?`, parentID)
	}

	var total int
	var err error
	if page > 0 && pageSize > 0 {
		total, err = q.Limit(pageSize).Offset((page - 1) * pageSize).SelectAndCount()
	} else {
		err = q.Select()
	}
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, 0, err
	}
	return perms, total, nil
}

func UpdatePermissionTx(tx *pg.Tx, id int64, name, code string, parentID int64, sort int) (*Permission, error) {
	perm := Permission{ID: id}
	if err := tx.Model(&perm).WherePK().Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	fullcode := code
	if parentID > 0 {
		parent := Permission{ID: parentID}
		if err := tx.Model(&parent).WherePK().Select(); err != nil {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
		fullcode = fmt.Sprintf("%s.%s", parent.FullCode, code)
	}
	q := tx.Model(&perm).WherePK().Set(`"updated_at"=CURRENT_TIMESTAMP`)

	if perm.Name != name {
		q = q.Set(`"name"=?`, name)
	}
	if perm.Code != code {
		q = q.Set(`"code"=?`, code).Set(`"is_menu"=?`, code == "menu")
	}
	if perm.FullCode != fullcode {
		q = q.Set(`"fullcode"=?`, fullcode)
	}
	if perm.ParentID != parentID {
		q = q.Set(`"parent_id"=?`, parentID)
	}
	if perm.Sort != sort {
		if _, err := tx.Model(&Permission{}).Where(`"parent_id"=?`, perm.ParentID).Where(`"sort">?`, perm.Sort).Set(`"sort"="sort"-1`).Update(); err != nil {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
		if _, err := tx.Model(&Permission{}).Where(`"parent_id"=?`, parentID).Where(`"sort">=?`, sort).Set(`"sort"="sort"+1`).Update(); err != nil {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
		q = q.Set(`"sort"=?`, sort)
	}

	if _, err := q.Returning(`*`).Update(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	/* 更新所有子权限的编码(fullcode) */
	if err := UpdatePermissionChildrenTx(tx, perm.ID); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	return &perm, nil
}

func UpdatePermissionChildrenTx(tx *pg.Tx, id int64) error {
	children := make([]*Permission, 0)
	if err := tx.Model(&children).Where(`"parent_id"=?`, id).Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return err
	}
	for _, child := range children {
		if _, err := UpdatePermissionTx(tx, child.ID, child.Name, child.Code, child.ParentID, child.Sort); err != nil {
			log.Errorf("DB Error: %v", err)
			return err
		}
	}
	return nil
}

func UpdatePermission(id int64, name, code string, parentID int64, sort int) (*Permission, error) {
	pg := db.PG()
	tx, err := pg.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	perm, err := UpdatePermissionTx(tx, id, name, code, parentID, sort)

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return perm, nil
}

func CreatePermission(name, code string, parentID int64, sort int, createdBy int64) (*Permission, error) {
	pg := db.PG()
	tx, err := pg.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	fullcode := code
	if parentID > 0 {
		parent := Permission{ID: parentID}
		if err := tx.Model(&parent).WherePK().Select(); err != nil {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
		fullcode = fmt.Sprintf("%s.%s", parent.FullCode, code)
	}

	if _, err := tx.Model(&Permission{}).Where(`"parent_id"=?`, parentID).Where(`"sort">=?`, sort).Set(`"sort"="sort"+1`).Update(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	perm := Permission{
		Name:      name,
		Code:      code,
		FullCode:  fullcode,
		ParentID:  parentID,
		CreatedBy: createdBy,
		IsMenu:    code == "menu",
		Sort:      sort,
	}
	if _, err := tx.Model(&perm).Insert(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return &perm, nil
}
