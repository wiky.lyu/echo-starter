package staff

import "time"

const (
	StaffStatusOK     = 1
	StaffStatusBanned = 2
)

type Staff struct {
	tableName struct{} `pg:"staff" json:"-"`

	ID       int64  `pg:"id,pk" json:"id"`
	Username string `pg:"username,notnull" json:"username"`
	Name     string `pg:"name,notnull" json:"name"`

	Salt     string `pg:"salt,notnull,use_zero" json:"-"`
	PType    string `pg:"ptype,notnull,use_zero" json:"-"`
	Password string `pg:"password,notnull,use_zero" json:"-"`

	Status      int  `pg:"status,notnull,use_zero" json:"status"`
	IsSuperuser bool `pg:"is_superuser,notnull,use_zero" json:"is_superuser"`

	Phone        string `pg:"phone,notnull,use_zero" json:"phone"`
	DepartmentID int64  `pg:"department_id,notnull,use_zero" json:"department_id"`

	CreatedBy int64     `pg:"created_by" json:"created_by"`
	CreatedAt time.Time `pg:"created_at" json:"created_at"`
	UpdatedAt time.Time `pg:"updated_at" json:"updated_at"`

	Department *Department `pg:"rel:has-one" json:"department,omitempty"`
}

const (
	StaffTokenStatusOK      = 1
	StaffTokenStatusInvalid = 2
)

type StaffToken struct {
	tableName struct{} `pg:"staff_token" json:"-"`

	ID      string `pg:"id,pk" json:"id"`
	StaffID int64  `pg:"staff_id,notnull" json:"staff_id"`
	Device  string `pg:"device,notnull,use_zero" json:"device"`
	IP      string `pg:"ip,notnull,use_zero" json:"ip"`
	Status  int    `pg:"status,notnull,use_zero" json:"status"`

	ExpiresAt time.Time `pg:"expires_at" json:"expires_at"`
	CreatedAt time.Time `pg:"created_at" json:"created_at"`
	UpdatedAt time.Time `pg:"updated_at" json:"updated_at"`

	Staff *Staff `pg:"rel:has-one" json:"staff"`
}

type Department struct {
	tableName struct{} `pg:"department" json:"-"`

	ID        int64     `pg:"id,pk" json:"id"`
	Name      string    `pg:"name,notnull" json:"name"`
	ParentID  int64     `pg:"parent_id,notnull" json:"parent_id"`
	CreatedBy int64     `pg:"created_by" json:"created_by"`
	CreatedAt time.Time `pg:"created_at" json:"created_at"`
	UpdatedAt time.Time `pg:"updated_at" json:"updated_at"`

	Children []*Department `pg:"-" json:"children"`
}

type Role struct {
	tableName struct{} `pg:"role" json:"-"`

	ID           int64     `pg:"id,pk" json:"id"`
	Name         string    `pg:"name,notnull" json:"name"`
	DepartmentID int64     `pg:"department_id,notnull" json:"department_id"`
	CreatedBy    int64     `pg:"created_by" json:"created_by"`
	CreatedAt    time.Time `pg:"created_at" json:"created_at"`
	UpdatedAt    time.Time `pg:"updated_at" json:"updated_at"`

	Department *Department `pg:"rel:has-one" json:"department,omitempty"`
}

const (
	RolePermissionScopeAll        = 0
	RolePermissionScopeDepartment = 10
	RolePermissionScopeSelf       = 20
)

type RolePermission struct {
	tableName struct{} `pg:"role_permission" json:"-"`

	ID     int64 `pg:"id,pk" json:"id"`
	RoleID int64 `pg:"role_id,notnull" json:"role_id"`
	PermID int64 `pg:"perm_id,notnull" json:"perm_id"`
	Scope  int   `pg:"scope,notnull,use_zero" json:"scope"`

	CreatedAt time.Time `pg:"created_at" json:"created_at"`
	UpdatedAt time.Time `pg:"updated_at" json:"updated_at"`
}

type StaffRole struct {
	tableName struct{} `pg:"staff_role" json:"-"`

	ID      int64 `pg:"id,pk" json:"id"`
	StaffID int64 `pg:"staff_id,notnull" json:"staff_id"`
	RoleID  int64 `pg:"role_id,notnull" json:"role_id"`

	CreatedAt time.Time `pg:"created_at" json:"created_at"`
	UpdatedAt time.Time `pg:"updated_at" json:"updated_at"`

	Role *Role `pg:"rel:has-one" json:"role"`
}
