package staff

import (
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/wiky.lyu/echo-starter/db"
)

func GetStaffToken(id string) (*StaffToken, error) {
	pg := db.PG()

	token := StaffToken{ID: id}
	if err := pg.Model(&token).WherePK().Select(); err != nil {
		if err != db.ErrNoRows {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &token, nil
}

func SetStaffTokenInvalid(staffID int64) error {
	pg := db.PG()

	if _, err := pg.Model((*StaffToken)(nil)).Where(`"staff_id"=?`, staffID).Set(`"status"=?`, StaffTokenStatusInvalid).Update(); err != nil {
		log.Errorf("DB Error: %v", err)
		return err
	}
	return nil
}

func CreateStaffToken(staffID int64, expiresAt time.Time, device, ip string) (*StaffToken, error) {
	pg := db.PG()
	tx, err := pg.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	if _, err := tx.Model((*StaffToken)(nil)).Where(`"staff_id"=?`, staffID).Where(`"status"=?`, StaffTokenStatusOK).Set(`"status"=?`, StaffTokenStatusInvalid).Update(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	token := StaffToken{
		StaffID:   staffID,
		Device:    device,
		IP:        ip,
		Status:    StaffTokenStatusOK,
		ExpiresAt: expiresAt,
	}
	if _, err := tx.Model(&token).Insert(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	return &token, nil
}

func FindStaffTokens(staffID int64, startTime, endTime time.Time, page, pageSize int) ([]*StaffToken, int, error) {
	pg := db.PG()

	tokens := make([]*StaffToken, 0)
	q := pg.Model(&tokens).Relation(`Staff`)
	if staffID > 0 {
		q = q.Where(`"staff_token"."staff_id"=?`, staffID)
	}
	if !startTime.IsZero() {
		q = q.Where(`"staff_token"."created_at">=?`, startTime)
	}
	if !endTime.IsZero() {
		q = q.Where(`"staff_token"."created_at"<?`, endTime)
	}

	if total, err := q.Limit(pageSize).Offset((page - 1) * pageSize).Order(`staff_token.created_at DESC`).SelectAndCountEstimate(100000); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, 0, err
	} else {
		return tokens, total, nil
	}
}
