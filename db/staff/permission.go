package staff

import (
	"github.com/go-pg/pg/v10"
	log "github.com/sirupsen/logrus"
	"gitlab.com/wiky.lyu/echo-starter/db"
	rbacModel "gitlab.com/wiky.lyu/echo-starter/db/rbac"
)

func IsGranted(staffID, permID int64) (*RolePermission, error) {
	pg := db.PG()

	rp := RolePermission{}

	if err := pg.Model(&rp).Join(`INNER JOIN "staff_role" AS "sr"`).JoinOn(`"role_permission"."role_id"="sr"."role_id"`).
		Where(`"sr"."staff_id"=?`, staffID).Where(`"role_permission"."perm_id"=?`, permID).
		Order(`scope`).Limit(1).Select(); err != nil {
		if err != db.ErrNoRows {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
		return nil, nil
	}

	return &rp, nil
}

func FindStaffMenus(staffID int64) ([]*rbacModel.Permission, error) {
	pg := db.PG()

	perms := make([]*rbacModel.Permission, 0)

	if err := pg.Model(&perms).Where(`"is_menu"`).Join(`INNER JOIN "role_permission" AS "rp"`).JoinOn(`"rp"."perm_id"="permission"."id"`).
		Join(`INNER JOIN "staff_role" AS "sr"`).JoinOn(`"sr"."role_id"="rp"."role_id"`).Where(`"sr"."staff_id"=?`, staffID).Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return perms, nil
}

func FindAllMenus() ([]*rbacModel.Permission, error) {
	pg := db.PG()

	perms := make([]*rbacModel.Permission, 0)

	if err := pg.Model(&perms).Where(`"is_menu"`).Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return perms, nil
}

func FindStaffPermissionsByCodes(staffID int64, codes []string) ([]*rbacModel.Permission, error) {
	perms := make([]*rbacModel.Permission, 0)

	if err := db.PG().Model(&perms).Where(`"permission"."fullcode" IN (?)`, pg.In(codes)).Join(`INNER JOIN "role_permission" AS "rp"`).JoinOn(`"rp"."perm_id"="permission"."id"`).
		Join(`INNER JOIN "staff_role" AS "sr"`).JoinOn(`"sr"."role_id"="rp"."role_id"`).Where(`"sr"."staff_id"=?`, staffID).Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return perms, nil
}

func FindAllPermissionsByCodes(codes []string) ([]*rbacModel.Permission, error) {
	perms := make([]*rbacModel.Permission, 0)

	if err := db.PG().Model(&perms).Where(`"permission"."fullcode" IN (?)`, pg.In(codes)).Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return perms, nil
}
