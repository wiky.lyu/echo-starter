package staff

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/wiky.lyu/echo-starter/db"
)

func FindDepartmentsByParent(parentID int64) ([]*Department, error) {
	pg := db.PG()

	departments := make([]*Department, 0)

	if err := pg.Model(&departments).Where(`"parent_id"=?`, parentID).Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return departments, nil
}

func CreateDepartment(name string, parentID int64, createdBy int64) (*Department, error) {
	pg := db.PG()
	tx, err := pg.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	if parentID > 0 {
		parent := Department{ID: parentID}
		if err := tx.Model(&parent).WherePK().Select(); err != nil {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
	}

	department := Department{
		Name:      name,
		ParentID:  parentID,
		CreatedBy: createdBy,
	}
	if _, err := tx.Model(&department).Insert(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return &department, nil
}

func UpdateDepartment(id int64, name string, parentID int64) (*Department, error) {
	pg := db.PG()
	tx, err := pg.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	if parentID > 0 {
		parent := Department{ID: parentID}
		if err := tx.Model(&parent).WherePK().Select(); err != nil {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
	}

	department := Department{ID: id}
	if err := tx.Model(&department).WherePK().Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if _, err := tx.Model(&department).WherePK().Set(`"updated_at"=CURRENT_TIMESTAMP`).
		Set(`"name"=?`, name).Set(`"parent_id"=?`, parentID).Returning(`*`).Update(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return &department, nil
}
