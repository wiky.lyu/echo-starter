package staff

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/wiky.lyu/echo-starter/db"
	"gitlab.com/wiky.lyu/echo-starter/x"
)

func GetStaff(id int64) (*Staff, error) {
	pg := db.PG()
	staff := Staff{ID: id}
	if err := pg.Model(&staff).WherePK().Select(); err != nil {
		if err != db.ErrNoRows {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &staff, nil
}

func UpdateMe(id int64, phone string) (*Staff, error) {
	pg := db.PG()

	staff := Staff{ID: id}
	if _, err := pg.Model(&staff).WherePK().Set(`"phone"=?`, phone).Returning(`*`).Update(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return &staff, nil
}

func GetStaffByUsername(username string) (*Staff, error) {
	pg := db.PG()
	staff := Staff{}
	if err := pg.Model(&staff).Where(`"username"=?`, username).Select(); err != nil {
		if err != db.ErrNoRows {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &staff, nil
}

func ResetStaffPassword(staffID int64, plain string) error {
	pg := db.PG()

	salt := x.RandomString(12)
	ptype := randomPType()
	password := encrypt(salt, plain, ptype)

	if _, err := pg.Model((*Staff)(nil)).Where(`"id"=?`, staffID).
		Set(`"salt"=?`, salt).Set(`"ptype"=?`, ptype).
		Set(`"password"=?`, password).Set(`"updated_at"=CURRENT_TIMESTAMP`).Update(); err != nil {
		log.Errorf("DB Error: %v", err)
		return err
	}

	return nil
}

/* 创建员工 */
func CreateStaff(username, name, phone, plain string, status int, departmentID, createdBy int64) (*Staff, error) {
	pg := db.PG()
	tx, err := pg.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	if departmentID > 0 {
		department := Department{ID: departmentID}
		if err := tx.Model(&department).WherePK().Select(); err != nil {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
	}

	staff := Staff{
		Username: username,
		Phone:    phone,
		Name:     name,
		Status:   status,

		Salt:         x.RandomString(12),
		PType:        randomPType(),
		DepartmentID: departmentID,

		IsSuperuser: false,
		CreatedBy:   createdBy,
	}

	staff.Password = encrypt(staff.Salt, plain, staff.PType)
	if _, err := tx.Model(&staff).Insert(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return &staff, nil
}

/* 更新员工信息 */
func UpdateStaff(id int64, name string, status int, phone, plain string, departmentID int64) (*Staff, error) {
	pg := db.PG()
	tx, err := pg.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	if departmentID > 0 {
		department := Department{ID: departmentID}
		if err := tx.Model(&department).WherePK().Select(); err != nil {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
	}

	staff := Staff{ID: id}
	if err := tx.Model(&staff).WherePK().Select(); err != nil {
		if err != db.ErrNoRows {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
		return nil, nil
	}

	q := tx.Model(&staff).WherePK()

	if plain != "" {
		salt := x.RandomString(12)
		ptype := randomPType()
		password := encrypt(salt, plain, ptype)
		q = q.Set(`"salt"=?`, salt).Set(`"ptype"=?`, ptype).Set(`"password"=?`, password)
	}

	if name != "" {
		q = q.Set(`"name"=?`, name)
	}

	if status > 0 {
		q = q.Set(`"status"=?`, status)
	}

	q.Set(`"phone"=?`, phone).Set(`"department_id"=?`, departmentID).Set(`"updated_at"=CURRENT_TIMESTAMP`)

	if _, err := q.Returning(`*`).Update(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	return &staff, nil
}

/* 获取员工列表 */
func FindStaffs(query string, status int, page, pageSize int) ([]*Staff, int, error) {
	pg := db.PG()

	staffs := make([]*Staff, 0)
	q := pg.Model(&staffs).Relation(`Department`)
	if query != "" {
		if x.IsDigit(query) {
			q = q.Where(`"staff"."phone"=?`, query)
		} else if x.IsChinese(query) {
			q = q.Where(`"staff"."name" LIKE ?`, "%"+query+"%")
		} else {
			q = q.Where(`"staff"."username"=?`, query)
		}
	}

	if status > 0 {
		q = q.Where(`"staff"."status"=?`, status)
	}

	if total, err := q.Offset((page - 1) * pageSize).Limit(pageSize).Order(`staff.created_at DESC`).SelectAndCountEstimate(10000); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, 0, err
	} else {
		return staffs, total, nil
	}
}

/* 获取第一个超级用户 */
func GetRootStaff() (*Staff, error) {
	pg := db.PG()
	staff := Staff{}
	if err := pg.Model(&staff).Where(`"is_superuser"`).Order(`id`).Limit(1).Select(); err != nil {
		if err != db.ErrNoRows {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &staff, nil
}

/* 创建超级用户 */
func CreateRootStaff(username, name, plain string) (*Staff, error) {
	pg := db.PG()
	tx, err := pg.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	staff := Staff{}
	if err := tx.Model(&staff).Where(`"is_superuser"`).Limit(1).Select(); err != nil {
		if err != db.ErrNoRows {
			log.Errorf("DB Error: %v", err)
			return nil, err
		}
	} else {
		return &staff, nil
	}

	staff = Staff{
		Username: username,
		Phone:    "",
		Name:     name,
		Status:   StaffStatusOK,

		Salt:  x.RandomString(12),
		PType: randomPType(),

		IsSuperuser: true,
	}

	staff.Password = encrypt(staff.Salt, plain, staff.PType)
	if _, err := tx.Model(&staff).Insert(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	staff.CreatedBy = staff.ID
	if _, err := tx.Model(&staff).WherePK().Set(`"created_by"=?`, staff.ID).Update(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return &staff, nil
}

func FindStaffRoles(staffID int64) ([]*StaffRole, error) {
	pg := db.PG()

	staffRoles := make([]*StaffRole, 0)
	if err := pg.Model(&staffRoles).Relation(`Role`).Relation(`Role.Department`).Where(`"staff_role"."staff_id"=?`, staffID).Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return staffRoles, nil
}

func UpdateStaffRoles(staffID int64, roleIDs []int64) error {
	tx, err := db.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return err
	}
	defer tx.Rollback()

	if _, err := tx.Model(&StaffRole{}).Where(`"staff_id"=?`, staffID).Delete(); err != nil {
		log.Errorf("DB Error: %v", err)
		return err
	}

	for _, roleID := range roleIDs {
		staffRole := StaffRole{
			StaffID: staffID,
			RoleID:  roleID,
		}
		if _, err := tx.Model(&staffRole).Insert(); err != nil {
			log.Errorf("DB Error: %v", err)
			return err
		}
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return err
	}

	return nil
}
