package staff

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/wiky.lyu/echo-starter/db"
	rbacModel "gitlab.com/wiky.lyu/echo-starter/db/rbac"
)

func FindRoles(query string, departmentID int64, page, pageSize int) ([]*Role, int, error) {
	pg := db.PG()

	roles := make([]*Role, 0)
	q := pg.Model(&roles).Relation(`Department`)

	if query != "" {
		q = q.Where(`"role"."name" LIKE ?`, "%"+query+"%")
	}
	if departmentID > 0 {
		q = q.Where(`"role"."department_id"=?`, departmentID)
	}

	total, err := q.Limit(pageSize).Offset((page - 1) * pageSize).SelectAndCount()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, 0, err
	}
	return roles, total, nil
}

func CreateRole(name string, departmentID int64, createdBy int64) (*Role, error) {
	tx, err := db.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	department := Department{ID: departmentID}
	if err := tx.Model(&department).WherePK().Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	role := Role{
		Name:         name,
		DepartmentID: departmentID,
		CreatedBy:    createdBy,
	}
	if _, err := tx.Model(&role).Insert(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	return &role, nil
}

func UpdateRole(id int64, name string, departmentID int64) (*Role, error) {
	tx, err := db.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	department := Department{ID: departmentID}
	if err := tx.Model(&department).WherePK().Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	role := Role{ID: id}
	if err := tx.Model(&role).WherePK().Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if _, err := tx.Model(&role).WherePK().Set(`"updated_at"=CURRENT_TIMESTAMP`).
		Set(`"name"=?`, name).Set(`"department_id"=?`, departmentID).Returning(`*`).Update(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	return &role, nil
}

/* 获取角色的所有权限 */
func FindRolePermissions(roleID int64) ([]*RolePermission, error) {
	pg := db.PG()
	rps := make([]*RolePermission, 0)

	if err := pg.Model(&rps).Where(`"role_id"=?`, roleID).Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return rps, nil
}

/* 更新角色权限 */
func UpdateRolePermissions(roleID int64, perms []*RolePermission) error {
	tx, err := db.Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return err
	}
	defer tx.Rollback()

	if err := tx.Model(&Role{ID: roleID}).WherePK().Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return err
	}

	if _, err := tx.Model(&RolePermission{}).Where(`"role_id"=?`, roleID).Delete(); err != nil {
		log.Errorf("DB Error: %v", err)
		return err
	}

	for _, perm := range perms {

		if err := tx.Model(&rbacModel.Permission{ID: perm.PermID}).WherePK().Select(); err != nil {
			log.Errorf("DB Error: %v", err)
			return err
		}

		rp := RolePermission{
			RoleID: roleID,
			PermID: perm.PermID,
			Scope:  perm.Scope,
		}
		if _, err := tx.Model(&rp).Insert(); err != nil {
			log.Errorf("DB Error: %v", err)
			return err
		}
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return err
	}
	return nil
}

func FindRolesByDepartment(departmentID int64) ([]*Role, error) {
	pg := db.PG()
	roles := make([]*Role, 0)

	if err := pg.Model(&roles).Relation(`Department`).Where(`"role"."department_id"=?`, departmentID).Select(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return roles, nil
}
