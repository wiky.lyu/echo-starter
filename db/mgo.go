package db

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var _mclient *mongo.Client
var _mdb *mongo.Database

func InitMongo(url, db string) error {
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	_mclient, err = mongo.Connect(ctx, options.Client().ApplyURI(url))
	if err != nil {
		return err
	}

	_mdb = _mclient.Database(db)
	return nil
}

func MC(c string) *mongo.Collection {
	return _mdb.Collection(c)
}

func Mongo() *mongo.Client {
	return _mclient
}
