package errors

const (
	OK                          = 0
	ErrStaffNotFound            = 10000
	ErrStaffPasswordIncorret    = 10001
	ErrStaffPermissionForbidden = 10002
	ErrStaffUsernameDuplicated  = 10003
	ErrStaffBanned              = 10004
	ErrRootStaffExists          = 10005

	ErrAPIDuplicated = 11000
)
