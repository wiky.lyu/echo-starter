CREATE DATABASE "starter" WITH ENCODING=UTF8;

\c "starter";

CREATE EXTENSION "uuid-ossp";

/* 部门 */
CREATE TABLE "department"(
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(64) NOT NULL,
    "parent_id" BIGINT NOT NULL DEFAULT 0,
    "created_by" BIGINT,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX ON "department"("parent_id");

/* 人员 */
CREATE TABLE "staff"(
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "username" VARCHAR(32) NOT NULL,
    "name" VARCHAR(32) NOT NULL,
    "salt" VARCHAR(32) NOT NULL,
    "ptype" VARCHAR(16) NOT NULL,
    "password" VARCHAR(256) NOT NULL,
    "status" SMALLINT NOT NULL DEFAULT 0,
    "is_superuser" BOOLEAN NOT NULL DEFAULT FALSE,
    "phone" VARCHAR(32) NOT NULL DEFAULT '',
    "department_id" BIGINT NOT NULL DEFAULT 0,
    "created_by" BIGINT,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    UNIQUE("username")
);
CREATE INDEX ON "staff"("name");
CREATE INDEX ON "staff"("status");
CREATE INDEX ON "staff"("created_at");

CREATE TABLE "staff_token"(
    "id" UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    "staff_id" BIGINT NOT NULL,
    "device" VARCHAR(1024) NOT NULL DEFAULT '',
    "ip" VARCHAR(256) NOT NULL DEFAULT '',
    "expires_at" TIMESTAMP WITH TIME ZONE,
    "status" INT NOT NULL DEFAULT 0,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX ON "staff_token"("staff_id","status");
CREATE INDEX ON "staff_token"("staff_id","expires_at");
CREATE INDEX ON "staff_token"("created_at");

/* 权限 */
CREATE TABLE "permission"(
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(128) NOT NULL,
    "code" VARCHAR(128) NOT NULL,
    "fullcode" VARCHAR(128) NOT NULL,
    "parent_id" BIGINT NOT NULL DEFAULT 0, -- 上级
    "is_menu" BOOLEAN NOT NULL DEFAULT FALSE, -- 是否是菜单权限
    "sort" SMALLINT NOT NULL DEFAULT 0, -- 排序
    "created_by" BIGINT,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    UNIQUE("fullcode")
);
CREATE INDEX ON "permission"("is_menu");
CREATE INDEX ON "permission"("sort");

/* 接口 */
CREATE TABLE "api"(
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "method" VARCHAR(32) NOT NULL,
    "path" VARCHAR(1024) NOT NULL,
    "perm_id" BIGINT NOT NULL,
    "created_by" BIGINT,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    UNIQUE("method","path")
);
CREATE INDEX ON "api"("method");

/* 角色 */
CREATE TABLE "role"(
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(128) NOT NULL,
    "department_id" BIGINT NOT NULL,
    "created_by" BIGINT,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX ON "role"("department_id");

/* 角色和权限的关联 */
CREATE TABLE "role_permission"(
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "role_id" BIGINT NOT NULL,
    "perm_id" BIGINT NOT NULL,
    "scope" SMALLINT NOT NULL,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    UNIQUE("role_id","perm_id")
);
CREATE INDEX ON "role_permission"("role_id");
CREATE INDEX ON "role_permission"("perm_id");
CREATE INDEX ON "role_permission"("scope");

/* 人员与角色的关联 */
CREATE TABLE "staff_role"(
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "staff_id" BIGINT NOT NULL,
    "role_id" BIGINT NOT NULL,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    UNIQUE("staff_id","role_id")
);
CREATE INDEX ON "staff_role"("staff_id");
CREATE INDEX ON "staff_role"("role_id");