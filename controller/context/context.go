package context

import (
	"bytes"
	"net/http"
	"reflect"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/tealeg/xlsx/v3"
	staffModel "gitlab.com/wiky.lyu/echo-starter/db/staff"
)

type Context struct {
	echo.Context

	Staff *staffModel.Staff
	RP    *staffModel.RolePermission
}

func (c *Context) IntParam(key string) int64 {
	v := c.Param(key)

	i, _ := strconv.ParseInt(v, 10, 64)
	return i
}

func (c *Context) IntFormParam(key string) int64 {
	v := c.FormValue(key)
	i, _ := strconv.ParseInt(v, 10, 64)
	return i
}

func (c *Context) Success(data interface{}) error {
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": 0,
		"data":   data,
	})
}

func (c *Context) Fail(status int, data interface{}) error {
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": status,
		"data":   data,
	})
}

func (c *Context) List(data interface{}, page, pageSize, total int) error {
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": 0,
		"data": map[string]interface{}{
			"page":      page,
			"page_size": pageSize,
			"total":     total,
			"list":      data,
		},
	})
}

func (c *Context) Bind(data interface{}) error {
	if err := c.Context.Bind(data); err != nil {
		return err
	}

	/* 过滤字符串的首尾空格 */
	v := reflect.ValueOf(data).Elem()
	for i := 0; i < v.NumField(); i++ {
		vv := v.Field(i)
		if vv.Kind() == reflect.String {
			vv.SetString(strings.TrimSpace(vv.String()))
		}
	}
	return nil
}

func (c *Context) BindAndValidate(data interface{}) error {
	if err := c.Bind(data); err != nil {
		return err
	}

	return c.Validate(data)
}

func (c *Context) BadRequest() error {
	return c.NoContent(http.StatusBadRequest)
}

func (c *Context) NotFound() error {
	return c.NoContent(http.StatusNotFound)
}

func (c *Context) InternalServerError() error {
	return c.NoContent(http.StatusInternalServerError)
}

func (c *Context) Unauthorized() error {
	return c.NoContent(http.StatusUnauthorized)
}

func (c *Context) Forbidden() error {
	return c.NoContent(http.StatusForbidden)
}

func (c *Context) XLSX(filename string, wb *xlsx.File) error {

	buf := bytes.NewBuffer(nil)
	if err := wb.Write(buf); err != nil {
		return err
	}

	c.Response().Header().Add("Content-Disposition", "attachment;filename="+filename+".xlsx")
	return c.Stream(200, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", buf)
}
