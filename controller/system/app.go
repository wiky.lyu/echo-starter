package system

import (
	"gitlab.com/wiky.lyu/echo-starter/controller/context"
	staffModel "gitlab.com/wiky.lyu/echo-starter/db/staff"

	"github.com/labstack/echo/v4"
)

type AppInfo struct {
	HasRoot bool `json:"has_root"`
}

func GetAppInfo(c echo.Context) error {
	ctx := c.(*context.Context)

	staff, err := staffModel.GetRootStaff()
	if err != nil {
		return ctx.InternalServerError()
	}
	info := AppInfo{
		HasRoot: staff != nil,
	}
	return ctx.Success(info)
}
