package system

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/wiky.lyu/echo-starter/controller/context"
	staffModel "gitlab.com/wiky.lyu/echo-starter/db/staff"
	"gitlab.com/wiky.lyu/echo-starter/errors"
)

type CreateRootStaffRequest struct {
	Username string `json:"username" form:"username" query:"username" validate:"gt=3"`
	Name     string `json:"name" form:"name" query:"name" validate:"gt=0"`
	Password string `json:"password" form:"password" query:"password" validate:"gt=5"`
}

func CreateRootStaff(c echo.Context) error {
	ctx := c.(*context.Context)

	req := CreateRootStaffRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}

	staff, err := staffModel.GetRootStaff()
	if err != nil {
		return ctx.InternalServerError()
	} else if staff != nil {
		return ctx.Fail(errors.ErrRootStaffExists, nil)
	}

	staff, err = staffModel.CreateRootStaff(req.Username, req.Name, req.Password)
	if err != nil {
		return ctx.InternalServerError()
	}

	return ctx.Success(staff)
}
