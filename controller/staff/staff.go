package staff

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/wiky.lyu/echo-starter/controller/context"
	staffModel "gitlab.com/wiky.lyu/echo-starter/db/staff"
	"gitlab.com/wiky.lyu/echo-starter/errors"
	"gitlab.com/wiky.lyu/echo-starter/x"
)

func GetMe(c echo.Context) error {
	ctx := c.(*context.Context)

	return ctx.Success(ctx.Staff)
}

type FindStaffsRequest struct {
	Query    string `json:"query" form:"query" query:"query"`
	Status   int    `json:"status" form:"status" query:"status"`
	Page     int    `json:"page" form:"page" query:"page"`
	PageSize int    `json:"page_size" form:"page_size" query:"page_size"`
}

func FindStaffs(c echo.Context) error {
	ctx := c.(*context.Context)

	req := FindStaffsRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}

	req.Page, req.PageSize = x.Pagination(req.Page, req.PageSize)
	staffs, total, err := staffModel.FindStaffs(req.Query, req.Status, req.Page, req.PageSize)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.List(staffs, req.Page, req.PageSize, total)
}

func GetStaff(c echo.Context) error {
	ctx := c.(*context.Context)

	id := ctx.IntParam("id")

	staff, err := staffModel.GetStaff(id)
	if err != nil {
		return ctx.InternalServerError()
	} else if staff == nil {
		return ctx.NotFound()
	}
	return ctx.Success(staff)
}

type CreateStaffRequest struct {
	Username     string `json:"username" form:"username" query:"username" validate:"gt=3"`
	Name         string `json:"name" form:"name" query:"name" validate:"gt=0"`
	Password     string `json:"password" form:"password" query:"password" validate:"gt=5"`
	DepartmentID int64  `json:"department_id" form:"department_id" query:"department_id"`
}

func CreateStaff(c echo.Context) error {
	ctx := c.(*context.Context)

	req := CreateStaffRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}

	staff, err := staffModel.GetStaffByUsername(req.Username)
	if err != nil {
		return ctx.InternalServerError()
	} else if staff != nil {
		return ctx.Fail(errors.ErrStaffUsernameDuplicated, nil)
	}

	staff, err = staffModel.CreateStaff(req.Username, req.Name, "", req.Password, staffModel.StaffStatusOK, req.DepartmentID, ctx.Staff.ID)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(staff)
}

type UpdateStaffRequest struct {
	Name         string `json:"name" form:"name" query:"name"`
	Password     string `json:"password" form:"password" query:"password"`
	DepartmentID int64  `json:"department_id" form:"department_id" query:"department_id"`
}

func UpdateStaff(c echo.Context) error {
	ctx := c.(*context.Context)

	id := ctx.IntParam("id")

	req := UpdateStaffRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}

	staff, err := staffModel.UpdateStaff(id, req.Name, 0, "", req.Password, req.DepartmentID)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(staff)
}

func FindStaffRoles(c echo.Context) error {
	ctx := c.(*context.Context)

	id := ctx.IntParam("id")

	staffRoles, err := staffModel.FindStaffRoles(id)
	if err != nil {
		return ctx.InternalServerError()
	}
	roles := make([]*staffModel.Role, 0)
	for _, staffRole := range staffRoles {
		roles = append(roles, staffRole.Role)
	}
	return ctx.Success(roles)
}

type UpdateStaffRolesRequest struct {
	RoleIDs []int64 `json:"role_ids" form:"role_ids" query:"role_ids"`
}

func UpdateStaffRoles(c echo.Context) error {
	ctx := c.(*context.Context)

	id := ctx.IntParam("id")

	req := UpdateStaffRolesRequest{}
	if err := ctx.Bind(&req); err != nil {
		return ctx.BadRequest()
	}

	if err := staffModel.UpdateStaffRoles(id, req.RoleIDs); err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(nil)
}

type UpdateStaffPasswordRequest struct {
	Old string `json:"old" form:"old" query:"old" validate:"gt=0"`
	New string `json:"new" form:"new" query:"new" validate:"gt=5"`
}

func UpdateMePassword(c echo.Context) error {
	ctx := c.(*context.Context)

	req := UpdateStaffPasswordRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}
	if !ctx.Staff.Auth(req.Old) {
		return ctx.Fail(errors.ErrStaffPasswordIncorret, nil)
	}
	err := staffModel.ResetStaffPassword(ctx.Staff.ID, req.New)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(nil)
}
