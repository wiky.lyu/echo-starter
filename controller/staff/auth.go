package staff

import (
	"net/http"
	"time"

	"gitlab.com/wiky.lyu/echo-starter/controller/context"
	"gitlab.com/wiky.lyu/echo-starter/errors"

	"github.com/gorilla/sessions"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	staffModel "gitlab.com/wiky.lyu/echo-starter/db/staff"
)

type LoginRequest struct {
	Username string `json:"username" query:"username" form:"username" validate:"required"`
	Password string `json:"password" query:"password" form:"password" validate:"required"`
}

func Login(c echo.Context) error {
	ctx := c.(*context.Context)
	req := LoginRequest{}
	if err := ctx.Bind(&req); err != nil {
		return ctx.BadRequest()
	} else if err := ctx.Validate(&req); err != nil {
		return ctx.BadRequest()
	}
	staff, err := staffModel.GetStaffByUsername(req.Username)
	if err != nil {
		return ctx.NoContent(http.StatusInternalServerError)
	} else if staff == nil {
		return ctx.Fail(errors.ErrStaffNotFound, nil)
	} else if !staff.IsSuperuser && staff.Status == staffModel.StaffStatusBanned {
		return ctx.Fail(errors.ErrStaffBanned, nil)
	}
	if !staff.Auth(req.Password) {
		return ctx.Fail(errors.ErrStaffPasswordIncorret, nil)
	}

	now := time.Now()
	token, err := staffModel.CreateStaffToken(staff.ID, now.AddDate(0, 0, 1), ctx.Request().UserAgent(), ctx.RealIP())
	if err != nil {
		return ctx.NoContent(http.StatusInternalServerError)
	}

	sess, _ := session.Get("staff", ctx)
	sess.Options = &sessions.Options{
		Path:   "/",
		MaxAge: 0,
	}

	sess.Values["token"] = token.ID
	sess.Save(ctx.Request(), ctx.Response())

	return ctx.Success(token)
}

func Logout(c echo.Context) error {
	ctx := c.(*context.Context)

	sess, _ := session.Get("staff", ctx)
	sess.Options = &sessions.Options{
		Path:   "/",
		MaxAge: 0,
	}

	sess.Values["token"] = ""
	sess.Save(ctx.Request(), ctx.Response())

	if err := staffModel.SetStaffTokenInvalid(ctx.Staff.ID); err != nil {
		return ctx.InternalServerError()
	}

	return ctx.Success(nil)
}
