package staff

import (
	"gitlab.com/wiky.lyu/echo-starter/controller/context"
	rbacModel "gitlab.com/wiky.lyu/echo-starter/db/rbac"
	staffModel "gitlab.com/wiky.lyu/echo-starter/db/staff"
	"gitlab.com/wiky.lyu/echo-starter/x"

	"github.com/labstack/echo/v4"
)

type FindRolesRequest struct {
	Query        string `json:"query" form:"query" query:"query"`
	DepartmentID int64  `json:"department_id" form:"department_id" query:"department_id"`
	Page         int    `json:"page" form:"page" query:"page"`
	PageSize     int    `json:"page_size" form:"page_size" query:"page_size"`
}

func FindRoles(c echo.Context) error {
	ctx := c.(*context.Context)
	req := FindRolesRequest{}

	if err := ctx.Bind(&req); err != nil {
		return ctx.BadRequest()
	}

	req.Page, req.PageSize = x.Pagination(req.Page, req.PageSize)
	roles, total, err := staffModel.FindRoles(req.Query, req.DepartmentID, req.Page, req.PageSize)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.List(roles, req.Page, req.PageSize, total)
}

type CreateRoleRequest struct {
	Name         string `json:"name" form:"name" query:"name" validate:"gt=0"`
	DepartmentID int64  `json:"department_id" form:"department_id" query:"department_id" validate:"gt=0"`
}

func CreateRole(c echo.Context) error {
	ctx := c.(*context.Context)
	req := CreateRoleRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}

	role, err := staffModel.CreateRole(req.Name, req.DepartmentID, ctx.Staff.ID)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(role)
}

type UpdateRoleRequest struct {
	Name         string `json:"name" form:"name" query:"name" validate:"gt=0"`
	DepartmentID int64  `json:"department_id" form:"department_id" query:"department_id" validate:"gt=0"`
}

func UpdateRole(c echo.Context) error {
	ctx := c.(*context.Context)

	id := ctx.IntParam("id")

	req := UpdateRoleRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}

	role, err := staffModel.UpdateRole(id, req.Name, req.DepartmentID)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(role)
}

type RolePermissionWrapper struct {
	*rbacModel.Permission

	Scope   int  `json:"scope"`
	Checked bool `json:"checked"`

	Children []*RolePermissionWrapper `json:"children"`
}

func FindRolePermissions(c echo.Context) error {
	ctx := c.(*context.Context)

	id := ctx.IntParam("id")

	perms, err := rbacModel.FindPermissionTree()
	if err != nil {
		return ctx.InternalServerError()
	}
	rps, err := staffModel.FindRolePermissions(id)
	if err != nil {
		return ctx.InternalServerError()
	}
	rpsMap := make(map[int64]int)
	for _, rp := range rps {
		rpsMap[rp.PermID] = rp.Scope
	}
	var findPerms func(perms []*rbacModel.Permission) []*RolePermissionWrapper
	findPerms = func(perms []*rbacModel.Permission) []*RolePermissionWrapper {
		result := make([]*RolePermissionWrapper, 0)
		for _, perm := range perms {
			wrapper := &RolePermissionWrapper{
				Permission: perm,
			}
			if scope, ok := rpsMap[perm.ID]; ok {
				wrapper.Checked = true
				wrapper.Scope = scope
			}
			wrapper.Children = findPerms(perm.Children)
			result = append(result, wrapper)
		}
		return result
	}
	result := findPerms(perms)
	return ctx.Success(result)
}

type UpdateRolePermissionsRequest struct {
	Perms []*staffModel.RolePermission `json:"perms" form:"perms" query:"perms"`
}

func UpdateRolePermissions(c echo.Context) error {
	ctx := c.(*context.Context)

	id := ctx.IntParam("id")

	req := UpdateRolePermissionsRequest{}
	if err := ctx.Bind(&req); err != nil {
		return ctx.BadRequest()
	}

	if err := staffModel.UpdateRolePermissions(id, req.Perms); err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(nil)
}
