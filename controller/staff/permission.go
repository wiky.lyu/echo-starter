package staff

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/wiky.lyu/echo-starter/controller/context"
	rbacModel "gitlab.com/wiky.lyu/echo-starter/db/rbac"
	staffModel "gitlab.com/wiky.lyu/echo-starter/db/staff"
)

func FindMenus(c echo.Context) error {
	ctx := c.(*context.Context)

	var perms []*rbacModel.Permission
	var err error

	if ctx.Staff.IsSuperuser {
		perms, err = staffModel.FindAllMenus()
	} else {
		perms, err = staffModel.FindStaffMenus(ctx.Staff.ID)
	}
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(perms)
}

type FindPermissionsRequest struct {
	Codes []string `json:"codes" form:"codes" query:"codes" validate:"gt=0"`
}

func FindPermissions(c echo.Context) error {
	ctx := c.(*context.Context)

	req := FindPermissionsRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.InternalServerError()
	}

	var perms []*rbacModel.Permission
	var err error

	if ctx.Staff.IsSuperuser {
		perms, err = staffModel.FindAllPermissionsByCodes(req.Codes)
	} else {
		perms, err = staffModel.FindStaffPermissionsByCodes(ctx.Staff.ID, req.Codes)
	}
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(perms)
}
