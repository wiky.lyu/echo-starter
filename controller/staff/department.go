package staff

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/wiky.lyu/echo-starter/controller/context"
	staffModel "gitlab.com/wiky.lyu/echo-starter/db/staff"
)

func findDepartmentChildren(department *staffModel.Department) {
	children, _ := staffModel.FindDepartmentsByParent(department.ID)
	if len(children) > 0 {
		for _, child := range children {
			findDepartmentChildren(child)
		}
	}
	department.Children = children
}

func FindDepartmentTree(c echo.Context) error {
	ctx := c.(*context.Context)

	departments, err := staffModel.FindDepartmentsByParent(0)
	if err != nil {
		return ctx.InternalServerError()
	}
	for _, department := range departments {
		findDepartmentChildren(department)
	}
	return ctx.Success(departments)
}

type CreateDepartmentRequest struct {
	Name     string `json:"name" form:"name" query:"name" validate:"gt=0"`
	ParentID int64  `json:"parent_id" form:"parent_id" query:"parent_id"`
}

func CreateDepartment(c echo.Context) error {
	ctx := c.(*context.Context)
	req := CreateDepartmentRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}

	department, err := staffModel.CreateDepartment(req.Name, req.ParentID, ctx.Staff.ID)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(department)
}

type UpdateDepartmentRequest struct {
	Name     string `json:"name" form:"name" query:"name" validate:"gt=0"`
	ParentID int64  `json:"parent_id" form:"parent_id" query:"parent_id"`
}

func UpdateDepartment(c echo.Context) error {
	ctx := c.(*context.Context)

	id := ctx.IntParam("id")

	req := UpdateDepartmentRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}

	department, err := staffModel.UpdateDepartment(id, req.Name, req.ParentID)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(department)
}

func FindDepartmentRoles(c echo.Context) error {
	ctx := c.(*context.Context)

	id := ctx.IntParam("id")

	roles, err := staffModel.FindRolesByDepartment(id)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(roles)
}
