package controller

import (
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"gitlab.com/wiky.lyu/echo-starter/controller/middleware"
	"gitlab.com/wiky.lyu/echo-starter/controller/rbac"
	"gitlab.com/wiky.lyu/echo-starter/controller/staff"
	"gitlab.com/wiky.lyu/echo-starter/controller/system"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func Register(e *echo.Echo) {
	e.Validator = &CustomValidator{validator: validator.New()}
	api := e.Group("/api", middleware.APIMiddleware)
	authAPI := api.Group("", middleware.StaffAuthMiddleware)
	rbacAPI := authAPI.Group("", middleware.RBACMiddleware)

	api.PUT("/login", staff.Login)

	api.GET("/system/appinfo", system.GetAppInfo)
	api.POST("/system/root", system.CreateRootStaff)

	authAPI.GET("/me", staff.GetMe)
	authAPI.PUT("/logout", staff.Logout)
	authAPI.GET("/me/menus", staff.FindMenus)
	authAPI.GET("/me/perms", staff.FindPermissions)
	authAPI.PUT("/me/password", staff.UpdateMePassword)

	authAPI.GET("/staff/:id", staff.GetStaff)
	rbacAPI.GET("/staffs", staff.FindStaffs)
	rbacAPI.POST("/staff", staff.CreateStaff)
	rbacAPI.PUT("/staff/:id", staff.UpdateStaff)
	authAPI.GET("/department/tree", staff.FindDepartmentTree)
	rbacAPI.POST("/department", staff.CreateDepartment)
	rbacAPI.PUT("/department/:id", staff.UpdateDepartment)
	authAPI.GET("/department/:id/roles", staff.FindDepartmentRoles)
	rbacAPI.GET("/roles", staff.FindRoles)
	rbacAPI.POST("/role", staff.CreateRole)
	rbacAPI.PUT("/role/:id", staff.UpdateRole)
	rbacAPI.GET("/role/:id/permissions", staff.FindRolePermissions)
	rbacAPI.PUT("/role/:id/permissions", staff.UpdateRolePermissions)
	authAPI.GET("/staff/:id/roles", staff.FindStaffRoles)
	rbacAPI.PUT("/staff/:id/roles", staff.UpdateStaffRoles)

	rbacAPI.GET("/rbac/permissions", rbac.FindPermissions)
	rbacAPI.GET("/rbac/permission/tree", rbac.FindPermissionTree)
	rbacAPI.POST("/rbac/permission", rbac.CreatePermission)
	rbacAPI.PUT("/rbac/permission/:id", rbac.UpdatePermission)
	rbacAPI.GET("/rbac/apis", rbac.FindAPIs)
	rbacAPI.POST("/rbac/api", rbac.CreateAPI)
	rbacAPI.PUT("/rbac/api/:id", rbac.UpdateAPI)
}
