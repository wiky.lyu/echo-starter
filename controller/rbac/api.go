package rbac

import (
	"gitlab.com/wiky.lyu/echo-starter/controller/context"
	rbacModel "gitlab.com/wiky.lyu/echo-starter/db/rbac"
	"gitlab.com/wiky.lyu/echo-starter/errors"
	"gitlab.com/wiky.lyu/echo-starter/x"

	"github.com/labstack/echo/v4"
)

type FindAPIsRequest struct {
	Query    string `json:"query" form:"query" query:"query"`
	Method   string `json:"method" form:"method" query:"method"`
	Page     int    `json:"page" form:"page" query:"page"`
	PageSize int    `json:"page_size" form:"page_size" query:"page_size"`
}

func FindAPIs(c echo.Context) error {
	ctx := c.(*context.Context)
	req := FindAPIsRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}

	req.Page, req.PageSize = x.Pagination(req.Page, req.PageSize)
	apis, total, err := rbacModel.FindAPIs(req.Query, req.Method, req.Page, req.PageSize)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.List(apis, req.Page, req.PageSize, total)
}

type CreateAPIRequest struct {
	Method string `json:"method" form:"method" query:"method" validate:"gt=0"`
	Path   string `json:"path" form:"path" query:"path" validate:"gt=0"`
	PermID int64  `json:"perm_id" form:"perm_id" query:"perm_id" validate:"gt=0"`
}

func CreateAPI(c echo.Context) error {
	ctx := c.(*context.Context)
	req := CreateAPIRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}

	api, err := rbacModel.GetAPIByMethodAndPath(req.Method, req.Path)
	if err != nil {
		return ctx.InternalServerError()
	} else if api != nil {
		return ctx.Fail(errors.ErrAPIDuplicated, nil)
	}

	api, err = rbacModel.CreateAPI(req.Method, req.Path, req.PermID, ctx.Staff.ID)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(api)
}

type UpdateAPIRequest struct {
	Method string `json:"method" form:"method" query:"method" validate:"gt=0"`
	Path   string `json:"path" form:"path" query:"path" validate:"gt=0"`
	PermID int64  `json:"perm_id" form:"perm_id" query:"perm_id" validate:"gt=0"`
}

func UpdateAPI(c echo.Context) error {
	ctx := c.(*context.Context)

	id := ctx.IntParam("id")

	req := UpdateAPIRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}

	api, err := rbacModel.GetAPIByMethodAndPath(req.Method, req.Path)
	if err != nil {
		return ctx.InternalServerError()
	} else if api != nil && api.ID != id {
		return ctx.Fail(errors.ErrAPIDuplicated, nil)
	}

	api, err = rbacModel.UpdateAPI(id, req.Method, req.Path, req.PermID)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(api)
}
