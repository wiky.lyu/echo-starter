package rbac

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/wiky.lyu/echo-starter/controller/context"
	rbacModel "gitlab.com/wiky.lyu/echo-starter/db/rbac"
)

type FindPermissionsRequest struct {
	Query    string `json:"query" form:"query" query:"query"`
	ParentID int64  `json:"parent_id" form:"parent_id" query:"parent_id"`
	Page     int    `json:"page" form:"page" query:"page"`
	PageSize int    `json:"page_size" form:"page_size" query:"page_size"`
}

func FindPermissions(c echo.Context) error {
	ctx := c.(*context.Context)
	req := FindPermissionsRequest{}
	if err := ctx.Bind(&req); err != nil {
		return ctx.BadRequest()
	}

	perms, total, err := rbacModel.FindPermissions(req.Query, req.ParentID, req.Page, req.PageSize)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.List(perms, req.Page, req.PageSize, total)
}

func FindPermissionTree(c echo.Context) error {
	ctx := c.(*context.Context)

	perms, err := rbacModel.FindPermissionTree()
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(perms)
}

type CreatePermissionRequest struct {
	Name     string `json:"name" form:"name" query:"name" validate:"gt=0"`
	Code     string `json:"code" form:"code" query:"code" validate:"gt=0"`
	ParentID int64  `json:"parent_id" form:"parent_id" query:"parent_id"`
	Sort     int    `json:"sort" form:"sort" query:"sort"`
}

func CreatePermission(c echo.Context) error {
	ctx := c.(*context.Context)
	req := CreatePermissionRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}

	perm, err := rbacModel.CreatePermission(req.Name, req.Code, req.ParentID, req.Sort, ctx.Staff.ID)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(perm)
}

type UpdatePermissionRequest struct {
	Name     string `json:"name" form:"name" query:"name" validate:"gt=0"`
	Code     string `json:"code" form:"code" query:"code" validate:"gt=0"`
	ParentID int64  `json:"parent_id" form:"parent_id" query:"parent_id"`
	Sort     int    `json:"sort" form:"sort" query:"sort"`
}

func UpdatePermission(c echo.Context) error {
	ctx := c.(*context.Context)
	id := ctx.IntParam("id")
	req := UpdatePermissionRequest{}
	if err := ctx.BindAndValidate(&req); err != nil {
		return ctx.BadRequest()
	}
	perm, err := rbacModel.UpdatePermission(id, req.Name, req.Code, req.ParentID, req.Sort)
	if err != nil {
		return ctx.InternalServerError()
	}
	return ctx.Success(perm)
}
