package middleware

import (
	"gitlab.com/wiky.lyu/echo-starter/controller/context"
	rbacModel "gitlab.com/wiky.lyu/echo-starter/db/rbac"
	staffModel "gitlab.com/wiky.lyu/echo-starter/db/staff"

	"github.com/labstack/echo/v4"
)

func RBACMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.(*context.Context)
		if ctx.Staff == nil {
			return ctx.Forbidden()
		}
		if !ctx.Staff.IsSuperuser {
			/* 用户不是超级用户的时候才判断权限 */
			method := ctx.Request().Method
			path := ctx.Path()

			api, err := rbacModel.GetAPIByMethodAndPath(method, path)
			if err != nil {
				return ctx.InternalServerError()
			} else if api == nil {
				return ctx.Forbidden()
			}
			if rp, err := staffModel.IsGranted(ctx.Staff.ID, api.PermID); err != nil {
				return ctx.InternalServerError()
			} else if rp == nil {
				return ctx.Forbidden()
			} else {
				ctx.RP = rp
			}
		} else {
			ctx.RP = &staffModel.RolePermission{RoleID: 0, PermID: 0, Scope: staffModel.RolePermissionScopeAll}
		}
		return next(ctx)
	}
}
