package middleware

import (
	"time"

	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"gitlab.com/wiky.lyu/echo-starter/controller/context"
	staffModel "gitlab.com/wiky.lyu/echo-starter/db/staff"
)

func getSessionToken(ctx *context.Context) string {
	sess, _ := session.Get("staff", ctx)
	if sess == nil {
		return ""
	}
	v, ok := sess.Values["token"]
	if !ok || v == nil {
		return ""
	}
	return v.(string)
}

func StaffAuthMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.(*context.Context)
		tokenID := getSessionToken(ctx)
		if tokenID == "" {
			return ctx.Unauthorized()
		}
		token, err := staffModel.GetStaffToken(tokenID)
		if err != nil {
			return err
		} else if token == nil || token.Status != staffModel.StaffTokenStatusOK {
			return ctx.Unauthorized()
		} else if !token.ExpiresAt.IsZero() && token.ExpiresAt.Before(time.Now()) { /* TOKEN已过期 */
			return ctx.Unauthorized()
		}
		staff, err := staffModel.GetStaff(token.StaffID)
		if err != nil {
			return err
		} else if staff == nil || (!staff.IsSuperuser && staff.Status == staffModel.StaffStatusBanned) {
			return ctx.Unauthorized()
		}
		ctx.Staff = staff
		return next(ctx)
	}
}
