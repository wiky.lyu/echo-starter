package middleware

import (
	"gitlab.com/wiky.lyu/echo-starter/controller/context"

	"github.com/labstack/echo/v4"
)

func APIMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := context.Context{Context: c}
		return next(&ctx)
	}
}
